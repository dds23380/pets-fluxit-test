package dspinazzola.pets;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class detailFragment extends Fragment {

    static private Long ID=0L;

    public detailFragment() {
        // Required empty public constructor
    }

    public static void setId(Long id){
        ID=id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_detail, container, false);
        final TextView tvId=view.findViewById(R.id.tvId);
        final TextView tvName=view.findViewById(R.id.tvName);
        final TextView tvCatId=view.findViewById(R.id.tvCatId);
        final TextView tvCatName=view.findViewById(R.id.tvCatName);
        final ListView lvTags=view.findViewById(R.id.listTags);
        final TextView tvStatus=view.findViewById(R.id.tvStatus);
        final ListView lvPhotoUrls=view.findViewById(R.id.listPhotoUrls);
        DBConnect.getInstance().findById(ID, new DBConnect.ResponseCallBack<ItemPetFull>() {
            @Override
            public void response(ItemPetFull response, String message) {
                if(response!=null){
                    try {
                        tvId.setText(String.valueOf(ID));
                    } catch (IllegalArgumentException e){
                        tvId.setText("");
                    }
                    try {
                        tvName.setText(response.name);
                    } catch (IllegalArgumentException e){
                        tvName.setText("");
                    }
                    if(response.category!=null) {
                        try {
                            tvCatId.setText(response.category.get("id"));
                        } catch (IllegalArgumentException e) {
                            tvCatId.setText("");
                        }
                        try {
                            tvCatName.setText(response.category.get("name"));
                        } catch (IllegalArgumentException e){
                            tvCatName.setText("");
                        }
                    }
                    if(response.tags!=null) {
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1);
                        for(ItemPetFull.datos item:response.tags){
                            String tmp="";
                            if(item.id!=null){
                                tmp=item.id;
                            }
                            if(item.name!=null){
                                tmp+=(" - "+item.name);
                            }
                            adapter.addAll(tmp);
                        }
                        lvTags.setAdapter(adapter);
                    }
                    tvStatus.setText(response.status);
                    if(response.photoUrls!=null){
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1);
                        adapter.addAll(response.photoUrls);
                        lvPhotoUrls.setAdapter(adapter);
                    }
                } else {
                    //Aquí si se produjo fallo
                    Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }

}
