package dspinazzola.pets;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public abstract class RVAdapter<T extends RecyclerView.ViewHolder, V> extends RecyclerView.Adapter<T>{

    private List<V> items;
    private final int layout;

    public RVAdapter(int layout) {
        items = new LinkedList<>();
        this.layout=layout;
        setHasStableIds(true);
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return createView(v);
    }

    public abstract T createView(View view);

    public abstract void bindView(T holder,V item);

    @Override
    public void onBindViewHolder(T holder, int position) {
        bindView(holder,items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public void insert(V item){
        items.add(item);
    }

    public void insertAll(List<V> items){
        this.items.addAll(items);
    }

    public V getItem(int position){
        if(position>=0&&position<getItemCount()){
            return items.get(position);
        }
        return null;
    }

    public void update(){
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void clear(){
        items.clear();
    }

// se utiliza para ordenar lista según la lógica recibida por el comparador
    public void setOrder(Comparator<V> comparator){
        Collections.sort(items,comparator);
    }

}
