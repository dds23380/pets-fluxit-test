package dspinazzola.pets;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Comparator;
import java.util.List;

public class MainActivity extends Activity {

    private static final int PICS[]={R.drawable.cat,R.drawable.dog,R.drawable.paw};
    private RVAdapter<mHolder,ItemPet> adapterPets;
    private RecyclerView rvPets;
    private SwipeRefreshLayout swipeContainer;
    private TextView tvLoading;
    private String history="";

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.asc:

                adapterPets.setOrder(new Comparator<ItemPet>() {
                    @Override
                    public int compare(ItemPet o1, ItemPet o2) {
                        return o1.name.compareTo(o2.name);
                    }
                });
                rvPets.setAdapter(adapterPets);
                return true;
            case R.id.desc:
                adapterPets.setOrder(new Comparator<ItemPet>() {
                    @Override
                    public int compare(ItemPet o1, ItemPet o2) {
                        return o2.name.compareTo(o1.name);
                    }
                });
                rvPets.setAdapter(adapterPets);
                return true;
            case R.id.find:
                AlertDialog.Builder dbuilder=new AlertDialog.Builder(this);
                final EditText mName=new EditText(this);
                dbuilder.setView(mName);
                if(!history.isEmpty()){
                    mName.setText(history);
                }
                dbuilder.setTitle("Find by name");
                dbuilder.setPositiveButton("find", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        history=mName.getText().toString();
                        find();
                    }
                });
                dbuilder.setNegativeButton("no", null);
                dbuilder.create().show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void find(){
        if(!history.isEmpty()){
            LinearLayoutManager myLayoutManager = (LinearLayoutManager) rvPets.getLayoutManager();
            int pos =0;
            if(myLayoutManager!=null){
                pos = myLayoutManager.findFirstVisibleItemPosition();
            }
            int size = adapterPets.getItemCount();
            for (int i = pos; i < size; i++) {
                if (adapterPets.getItem(i).name.contains(history)) {
                    rvPets.scrollToPosition(i);
                    break;
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DBConnect.getInstance().initialize();
        swipeContainer = findViewById(R.id.swipeContainer);
        tvLoading = findViewById(R.id.tvLoading);
        rvPets=findViewById(R.id.rvPets);
        rvPets.setHasFixedSize(true);
        rvPets.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        adapterPets=new RVAdapter<mHolder, ItemPet>(R.layout.item_pet) { //RVAdapter es un adaptador de RecyclerView (es personalizado)
            @Override
            public mHolder createView(View view) {
                mHolder holder=new mHolder(view);
                holder.id=view.findViewById(R.id.tvId);
                holder.name=view.findViewById(R.id.tvName);
                holder.pic=view.findViewById(R.id.ivPet);
                return holder;
            }

            @Override
            public void bindView(mHolder holder, final ItemPet item) {
                if(!holder.isInflated) {
                    try {
                        holder.id.setText(String.valueOf(item.id));
                    } catch ( IllegalArgumentException e){
                        holder.id.setText("");
                    }
                    try {
                        holder.name.setText(item.name);
                    } catch ( IllegalArgumentException e){
                        holder.name.setText("");
                    }
                    if(item.name.contains("kitt")){
                        holder.pic.setBackgroundResource(PICS[0]);
                    } else if(item.name.contains("dog")){
                        holder.pic.setBackgroundResource(PICS[1]);
                    } else {
                        holder.pic.setBackgroundResource(PICS[2]);
                    }
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(MainActivity.this,DetailedActivity.class);
                            DetailedActivity.ID=item.id;
                            DetailedActivity.NAME=item.name;
                            startActivity(intent);
                        }
                    });
                    holder.isInflated=true;
                }
            }
        };
        //
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DBConnect.ResponseCallBack<List<ItemPet>> callBack=new DBConnect.ResponseCallBack<List<ItemPet>>() {
                    @Override
                    public void response(List<ItemPet> response, String message) {
                        manageResponse(response,message);
                        swipeContainer.setRefreshing(false);
                    }
                };
                DBConnect.getInstance().findByStatus("available",callBack);
            }
        });
        swipeContainer.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        populatePets();
    }

    private void populatePets(){ //poblar recycler con una llamada asincrónica al servicio retrofit
        tvLoading.setVisibility(View.VISIBLE);
        DBConnect.ResponseCallBack<List<ItemPet>> callBack=new DBConnect.ResponseCallBack<List<ItemPet>>() {
            @Override
            public void response(List<ItemPet> response, String message) {
                manageResponse(response,message);
                tvLoading.setVisibility(View.GONE);
            }
        };
        DBConnect.getInstance().findByStatus("available",callBack);
    }

    private void manageResponse(List<ItemPet> response, String message){ //manejamos la respuesta del servicio de Retrofit
        String msg=message;
        if(response!=null){
            adapterPets.clear();
            adapterPets.insertAll(response);
            if(message==null) {
                msg="OK";
            }
            adapterPets.update();
            rvPets.setAdapter(adapterPets);
        }
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }

    private class mHolder extends RecyclerView.ViewHolder{
        public TextView id;
        public TextView name;
        public ImageView pic;
        public mHolder(View itemView) {
            super(itemView);
            itemView.setOnCreateContextMenuListener(MainActivity.this);
        }
        public boolean isInflated=false;
    }

}

