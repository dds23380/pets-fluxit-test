package dspinazzola.pets;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DetailedActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private TabAdapter tabAdapter;

    public static String NAME="";
    public static Long ID=0L;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
        getSupportActionBar().hide();
        detailFragment.setId(ID);
        ViewPager pager=findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        tabAdapter=new TabAdapter(getSupportFragmentManager());
        detailFragment frag1=new detailFragment();
        //fragmento con webview
        WebBrowser frag2=new WebBrowser();
        frag2.setBaseUrl("https://www.google.com/");
        frag2.setDataUrl("search?q="+NAME+"&ie=UTF-8");
        //agrego fragmentos al adaptador de tabs
        tabAdapter.addFragment(frag1,"Detail");
        tabAdapter.addFragment(frag2,"Google");
        pager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(pager);
        findViewById(R.id.ibBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}


