package dspinazzola.pets;

import android.support.annotation.NonNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

//Se utiliza Singleton Pattern
public class DBConnect {
    private static final DBConnect ourInstance = new DBConnect();

    private PetsInterfaces pets=null;

    public static DBConnect getInstance() {
        return ourInstance;
    }

    private DBConnect() {
    }

    public void findByStatus(String status, @NonNull final ResponseCallBack<List<ItemPet>> responseCB) {
        //Si no se inicializó el servicio descarta llamada
        if(pets==null){
            return;
        }
        Call<List<ItemPet>> rsp= pets.findByStatus(status);
        rsp.enqueue(new Callback<List<ItemPet>>() {
            @Override
            public void onResponse(Call<List<ItemPet>> call, Response<List<ItemPet>> response) {
                List<ItemPet> list =response.body();
                //llamada a callback de respuesta
                responseCB.response(list,response.message());
            }

            @Override
            public void onFailure(Call<List<ItemPet>> call, Throwable t) {
                //llamada a callback de respuesta con fallo
                responseCB.response(null,t.getMessage());
            }
        });
    }

    public void findById(Long id, @NonNull final ResponseCallBack<ItemPetFull> responseCB){
        //Si no se inicializó el servicio descarta llamada
        if(pets==null){
            return;
        }
        Call<ItemPetFull> rsp= pets.find(id);
        rsp.enqueue(new Callback<ItemPetFull>() {
            @Override
            public void onResponse(Call<ItemPetFull> call, Response<ItemPetFull> response) {
                //el Body de Response contiene el objeto deseado
                ItemPetFull pet=response.body();
                //llamada a callback de respuesta
                responseCB.response(pet,response.message());
            }

            @Override
            public void onFailure(Call<ItemPetFull> call, Throwable t) {
                //llamada a callback de respuesta con fallo
                responseCB.response(null,t.getMessage());
            }
        });
    }

    //interfaz pública utilizada como respuesta, es útil en llamadas asincrónicas.
    public interface ResponseCallBack<T>{
        public void response(T response, String message);
    }


    private interface PetsInterfaces {
        @GET("pet/findByStatus")
        Call<List<ItemPet>> findByStatus(@Query("status") String status);

        @GET("pet/{petId}")
        Call<ItemPetFull> find(@Path("petId") Long id);
    }

    public void initialize()  {
        //Inicializa servicio de Retrofit (es necesario llamar este método antes de usar el servicio)
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://petstore.swagger.io/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        pets = retrofit.create(PetsInterfaces.class);
    }

}
