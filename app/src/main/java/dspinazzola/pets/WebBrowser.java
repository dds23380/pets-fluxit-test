package dspinazzola.pets;

import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;


public class WebBrowser extends Fragment {
    private WebView webview;

    private final String failed_conexion="<div style=\"position: absolute;margin: auto;top: 30%\"><div><p style=\"text-align:center;\"><font size=\"5\" >" +
            "<strong>Imposible acceder al sitio web.<br>Verifique su conexión a internet e intente " +
            "nuevamente.</strong></font></p></div></div>";
    private boolean errorpage=false;

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    interface message {
        void show(final WebView view);
    }

    private static message msg=null;
    private boolean firstmsg=false;
    private String urlpage;
    private boolean noload;
    private String baseUrl="";
    private String dataUrl="";


    static void setMsg(message msg){
        WebBrowser.msg =msg;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.webbrowser, container, false);
        firstmsg=false;
        webview = view.findViewById(R.id.webview);
        //webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDisplayZoomControls(false);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webview.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(final WebView view, String url) {
                super.onPageFinished(view, url);
                if(noload) {
                    noload=false;
                    return;
                }
                if(!errorpage) {
                    if (msg != null && !firstmsg) {
                        msg.show(view);
                        firstmsg = true;
                    }
                }
                else {
                    noload=true;
                    webview.loadDataWithBaseURL(null, failed_conexion, "text/html", "utf-8", null);
                }
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                           SslError error) {
                handler.proceed();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                errorpage=true;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)  {
                errorpage=true;
            }

        });
        urlpage=baseUrl+dataUrl;
        webview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return false;
            }
        });
        reload(null);
        return view;
    }

    public void reload(View view){
        errorpage=false;
        if(urlpage!=null&&!urlpage.isEmpty()) {
            noload=false;
            webview.loadUrl(urlpage);
        }
        else {
            noload=true;
            webview.loadDataWithBaseURL(null, failed_conexion, "text/html", "utf-8", null);
        }
    }

}
