package dspinazzola.pets;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class ItemPetFull extends ItemPet{

     @SerializedName("category")
    public Map<String,String> category;

    @SerializedName("photoUrls")
    public List<String> photoUrls;

    @SerializedName("tags")
    public List<datos> tags;

    @SerializedName("status")
    public String status;

    public class datos{
        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;
    }

}
